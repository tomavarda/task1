import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { data } from './data';

export interface TreeData {
  id: string;
  retired: boolean;
  location: string;
  auditTrail: AuditTrail[];
  children?: TreeData[];
}

interface AuditTrail {
  userId: string;
  timestamp: string;
}

interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-folder-name-validator',
  templateUrl: './folder-name-validator.component.html',
  styleUrls: ['./folder-name-validator.component.scss'],
})
export class FolderNameValidatorComponent implements OnInit {

  private _transformer = (node: TreeData, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.id,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  readonly createFolder: FormGroup;
  readonly treeData = data;
  readonly inputVisible = false;

  constructor(private fb: FormBuilder) {
    this.dataSource.data = data;
    this.createFolder = fb.group({
      id: new FormControl('', this.validateNameFolder(this.treeData)),
    });
  }

  ngOnInit(): void {
  }

  validateNameFolder(data): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      let result;

      for (let item of data) {
        if (item.id === control.value || control.value === '') {
          result = true;
        }

        if (item.children.length) {
          const funcName = item => {
            result = null;
            return this.validateNameFolder(item.children);
          }
        }
      }

      return result ? { 'Name Folder already exist': true } : null;
    }
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

}

