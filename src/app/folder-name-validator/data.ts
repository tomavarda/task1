export const data = [
  {
    "id": "foler1",
    "retired": false,
    "location": "",
    "auditTrail": [
      {
        "userId": "a62b8efa-a303-43c9-9c22-6d3ad70f77f5",
        "timestamp": "2021-02-23T11:03:30.690074"
      }
    ],
    "children": [
      {
        "id": "subfolder1",
        "retired": false,
        "location": "/foler1",
        "auditTrail": [
          {
            "userId": "a62b8efa-a303-43c9-9c22-6d3ad70f77f5",
            "timestamp": "2021-02-23T11:03:38.04341"
          }
        ],
        "children": [
          {
            "id": "subsubfolder2",
            "retired": false,
            "location": "/foler1/subfolder1",
            "auditTrail": [
              {
                "userId": "a62b8efa-a303-43c9-9c22-6d3ad70f77f5",
                "timestamp": "2021-02-23T11:04:55.433443"
              }
            ],
            "children": []
          },
          {
            "id": "subsubfolder1",
            "retired": false,
            "location": "/foler1/subfolder1",
            "auditTrail": [
              {
                "userId": "a62b8efa-a303-43c9-9c22-6d3ad70f77f5",
                "timestamp": "2021-02-23T11:03:45.279609"
              }
            ],
            "children": [
              {
                "id": "test2",
                "retired": false,
                "location": "/foler1/subfolder1/subsubfolder1",
                "auditTrail": [
                  {
                    "userId": "a62b8efa-a303-43c9-9c22-6d3ad70f77f5",
                    "timestamp": "2021-02-23T11:04:35.497134"
                  }
                ],
                "children": []
              },
              {
                "id": "nestedFolder1",
                "retired": false,
                "location": "/foler1/subfolder1/subsubfolder1",
                "auditTrail": [
                  {
                    "userId": "a62b8efa-a303-43c9-9c22-6d3ad70f77f5",
                    "timestamp": "2021-02-23T11:04:06.255249"
                  }
                ],
                "children": []
              },
              {
                "id": "test1",
                "retired": false,
                "location": "/foler1/subfolder1/subsubfolder1",
                "auditTrail": [
                  {
                    "userId": "a62b8efa-a303-43c9-9c22-6d3ad70f77f5",
                    "timestamp": "2021-02-23T11:04:31.662038"
                  }
                ],
                "children": []
              }
            ]
          }
        ]
      }
    ]
  }
]
