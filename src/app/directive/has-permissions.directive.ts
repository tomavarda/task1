import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { PermissionsEnum } from '../models/app.model';

@Directive({
  selector: '[hasPermissions]'
})

export class HasPermissionsDirective implements OnInit{
  private getPermissions: PermissionsEnum[] = [];
  private operators: string = '';
  private readonly permissionList = [];

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {
    this.permissionList = Object.values(PermissionsEnum);
    this.permissionList.slice(0, (this.permissionList.length)/2);
  }

  @Input('hasPermissionsOperator') set hasPermissionsOperator(operator: string){
    this.operators = operator;
  };

  @Input() set hasPermissions(permission) {
    this.getPermissions = permission;
  }

  result(): boolean {
    return this.operators === 'AND'
      ? this.getPermissions.every(permission => this.permissionList.includes(permission))
      : this.getPermissions.some(permission => this.permissionList.includes(permission))
  }


  ngOnInit() {
    if (this.result()) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }

}
