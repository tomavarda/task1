import { AfterViewInit, ChangeDetectorRef, Component, Input, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-file-management',
  templateUrl: './file-management.component.html',
  styleUrls: ['./file-management.component.scss']
})
export class FileManagementComponent implements AfterViewInit{

  @ViewChild('viewContainer', { read: ViewContainerRef }) viewContainer: ViewContainerRef;
  @Input() template: TemplateRef<any>;

  readonly fileUploadForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private cdr: ChangeDetectorRef) {
    this.fileUploadForm = this.fb.group({
      file: null,
    });
  }

  ngAfterViewInit(): void {
    this.viewContainer.createEmbeddedView(this.template);
    this.cdr.detectChanges();
  }

  uploadFile(event): void {
    const fileTypes = ['pdf', 'jpg', 'jpeg', 'vnd.openxmlformats-officedocument.wordprocessingml.document', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    const file = event.target.files[0];
    const type = event.target.files[0].type;
    const i = type.lastIndexOf('/');
    const allowedTypes = fileTypes.includes(type.slice(i + 1));
    const sizeInMb = file.size / 1048576;

    if (sizeInMb >= 1 && allowedTypes === false) {
      this.openSnackBar('Your file size is more than 1MB or invalid type. Please, choose another file');
      this.fileUploadForm.reset();
    } else {
      this.openSnackBar(`Your file ${file.name} successfully upload`);
    }
  }

  openSnackBar(message: string): void {
    this.snackBar.open(message, '', {
      duration: 4500,
      horizontalPosition: 'start',
    });
  }

}
