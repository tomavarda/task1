import { Component } from '@angular/core';
import { PermissionsEnum, PermissionsEnum2 } from './models/app.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-task1';

  permissions = PermissionsEnum;
  permission2 = PermissionsEnum2;
}
